<?php

namespace App\User;
if(!isset($_SESSION))
{
    session_start();
}
class Info2 {
public static function info2($info2=NULL){
        if(is_null($info2)){
            $_info2=self::getinfo2();
            return $_info2;
            
        }
        else {
            self::setinfo2($info2);
        }
           
    }
    
    public static function setinfo2($info2){
        $_SESSION['info2']=$info2;
    }
    
    public static function getinfo2(){
        $_info2=$_SESSION['info2'];
        //$_SESSION['message']="";
        return $_info2;
    }
    }