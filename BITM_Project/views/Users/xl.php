<?php
include_once ('../../vendor/phpoffice/phpexcel/classes/PHPExcel.php');
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
//error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
include_once ('../../vendor/autoload.php');
use App\Admin\Users;
session_start();
use App\User\Info;
$id=Info::info();
$obj= new Users();
if(Info::info()!=NULL){
$allInfo= $obj->index();

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
//require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Serial')
            ->setCellValue('B1', 'ID')
            ->setCellValue('C1', 'Username')
            ->setCellValue('D1', 'Is_Admin')
            ->setCellValue('E1', 'Is_Active')
            ->setCellValue('F1', 'Email Address');
$cell_incrementer=2; 
$serial=0;
foreach($allInfo as $info):
    $serial++;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$cell_incrementer,$serial)
            ->setCellValue('B'.$cell_incrementer, $info['id'])
            ->setCellValue('C'.$cell_incrementer, $info['username'])
            ->setCellValue('D'.$cell_incrementer, $info['is_admin'])
            ->setCellValue('E'.$cell_incrementer, $info['is_active'])
            ->setCellValue('F'.$cell_incrementer, $info['email']);
        
$cell_incrementer++;
            endforeach;           

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
}
else {
header('Location:../User_info/index.php');
 }