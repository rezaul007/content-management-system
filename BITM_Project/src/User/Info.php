<?php

namespace App\User;
if(!isset($_SESSION))
{
    session_start();
}
class Info {
    public static function info($info=NULL){
        if(is_null($info)){
            $_info=self::getinfo();
            return $_info;
            
        }
        else {
            self::setinfo($info);
        }
           
    }
    
    public static function setinfo($info){
        $_SESSION['info']=$info;
    }
    
    public static function getinfo(){
        $_info=$_SESSION['info'];
        //$_SESSION['message']="";
        return $_info;
    }
    
}
