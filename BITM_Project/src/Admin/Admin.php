<?php

namespace App\Admin;
use App\Admin\Utility;
use App\Admin\Message;
use PDO;
class Admin{
    public $id="";
    public $url="";
    public $search="";
    public $title="";
    public $uniqid="";
    public $conn;
    public $user='root';
    public $pass='';
    public $deleted_at=""; 
    
    public function __construct() {
        $this->conn = new PDO('mysql:host=localhost;dbname=owncms', $this->user, $this->pass);
        
    }
    public function prepare($data=array()){
        if(is_array($data) && array_key_exists('title', $data)){
            $this->title=$data['title'];
        }
        if(array_key_exists('id',$data) && !empty($data['id'])){
            $this->id=$data['id'];
        }
        if(array_key_exists('uniqid',$data) && !empty($data['uniqid'])){
            $this->uniqid=$data['uniqid'];
        }
        
        if(is_array($data) && array_key_exists('url',$data)){
             $this->url=$data['url'];
        }
//        if(is_array($data) && array_key_exists('parent_id',$data)){
//             $this->parent_id=$data['parent_id'];
//        }
       if(array_key_exists('deleted_at', $data) && !empty($data['deleted_at'])){
           $this->deleted_at=$data['deleted_at'];
       }
       if (array_key_exists('search', $data) && !empty($data['search'])) {
            $this->search = $data['search'];
        }
      return $this;
//       var_dump($this);
//       die();
    }
    public function index() {
//        var_dump($_GET);
//        die();
        $allData = array();
        $whereClause=" 1=1 ";
        
        if(!empty($this->search)){
            $whereClause.="AND `title` LIKE '%{$this->search}%'";

        }

        
        $query = "SELECT * FROM `owncms`.`menus` WHERE `deleted_at` IS NULL AND".$whereClause;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
//        var_dump($allData);
//        die();
    }
    public function index2() {//sinlg row mail
        $query = "SELECT * FROM `owncms`.`menus` WHERE `menus`.`id` = :id";
        
        $result = $this->conn->prepare($query);
        
        $result->execute(array(':id'=>$this->id));
      
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        
        return $allData;
    }
    public function select_category() {//sinlg row mail
        $query = "SELECT * FROM `owncms`.`category` WHERE `category`.`deleted_at` IS NULL";
        
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
    }
    
    
     public function store() {
//         var_dump($_POST);
//         die();
         $id=$_POST['id'];
         $uniq_id=  uniqid($id);
             if (!empty($this->url) ) {
            $query = "INSERT INTO `owncms`.`menus` (`title`, `url`,`uniqid`) VALUES (:title,:url,:uniqid)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':title'=>$this->title,':url'=>$this->url,':uniqid'=>$uniq_id));

            if ($result) {
                Message::message("Data has been stored successfully");
                Utility::redirect();
            }
            else {
            Utility::redirect();
        }
             }
        
        else {
            Utility::redirect();
        }
            
    }
    
     public function edit() {
         $query = "SELECT * FROM `owncms`.`menus` WHERE `uniqid`=:uniqid ";
       
        $result = $this->conn->prepare( $query);
        $result->execute(array(':uniqid'=>$this->uniqid));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
       
    }
    
     public function update() {
        
        if(!empty($this->title)&& !empty($this->url))  {
         $query = "UPDATE `owncms`.`menus` SET `title`=:title,`url` =:url WHERE `menus`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':title'=>$this->title,':url'=>$this->url));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
        }
        elseif(!empty($this->title))  {
         $query = "UPDATE `owncms`.`menus` SET `title`=:title WHERE `menus`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':title'=>$this->title));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
        }
        elseif( !empty($this->url))  {
         $query = "UPDATE `owncms`.`menus` SET `url` =:url WHERE `menus`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':url'=>$this->url));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
        }
     elseif(empty($this->title)&& empty($this->url)) {
        Utility::redirect();
        }
 else {
     Utility::redirect();
 }

 
    }
    
     public function delete($data="") {
       $query = "DELETE FROM `owncms`.`menus` WHERE `menus`.`uniqid` =:id" ;
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->uniqid));
        //var_dump($result);
        //die();
        if($result){
            Message::message("Data has bee deleted successfully");
            Utility::redirect();
        }
        else{
            Utility::redirect();
        }
         
         
     }
    
     public function show() {
         
         $query = "SELECT * FROM `owncms`.`menus` WHERE `uniqid`=:id";
       
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$this->uniqid));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    public function trash() {
        $this->deleted_at=  time();
        $query = "UPDATE `owncms`.`menus` SET `deleted_at`=:deleted_at WHERE `menus`.`uniqid` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->uniqid,':deleted_at'=>$this->deleted_at));
         if($result){
         Message::message("Data has been trashed successfully");
         Utility::redirect();
       }
      
       
        else {
           Utility::redirect();
        }
    }
    public function trashed() {
        $allData = array();
        $query = "SELECT * FROM `owncms`.`menus` WHERE `deleted_at` IS NOT NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
    }
    public function recover() {
//        var_dump($_GET);
//        die();
        $query = "UPDATE `owncms`.`menus` SET `deleted_at`=:deleted_at WHERE `menus`.`uniqid` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->uniqid,':deleted_at'=>NULL));
         if($result){
         Message::message("Data has been recovered successfully");
         Utility::redirect();
       }
      
       
        else {
           Utility::redirect();
        }
    }
    public function getALLtitle(){
        $allData = array();
        $query = "SELECT `title` FROM `owncms`.`menus` WHERE `deleted_at` IS NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;

    }
    public function logout() {
        session_destroy();
        
       session_unset(Info::info('id'));
        Utility::redirect();
    }
}

