-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2016 at 07:03 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `owncms`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `summary` varchar(512) DEFAULT NULL,
  `html_summary` varchar(255) DEFAULT NULL,
  `detaile` varchar(512) DEFAULT NULL,
  `html_detaile` varchar(512) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  `publish_status` int(11) DEFAULT NULL,
  `uniqid` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `title`, `sub_title`, `user_id`, `summary`, `html_summary`, `detaile`, `html_detaile`, `deleted_at`, `publish_status`, `uniqid`, `url`, `image`, `created_at`) VALUES
(18, 'Paul Revere Biography', 'Demo', 17, 'Silversmith Paul Revere took part in the Boston Tea Party and famously alerted the Lexington Minutemen about the approach of the British in 1775.', 'Silversmith Paul Revere took part in the Boston Tea Party and famously alerted the Lexington Minutemen about the approach of the British in 1775.', 'just demo', 'just demo', NULL, 1, '57290adf5861e', '', '1462450496f1.png', NULL),
(19, 'New_Demo', 'Demo', 17, 'A river is a natural flowing watercourse, usually freshwater, flowing towards an ocean, sea, lake or another river. In some cases a river flows into the ground and becomes dry at the end of its course without reaching another body of water. Small rivers can be referred to using names such as stream, creek, brook, rivulet, and rill. There are no official definitions for the generic term river as applied to geographic features,[1] although in some countries or communities a stream is defined by its size. Many', '<p><strong>A river is a natural flowing <a title="Watercourse" href="https://en.wikipedia.org/wiki/Watercourse">watercourse</a>, usually <a class="mw-redirect" title="Freshwater" href="https://en.wikipedia.org/wiki/Freshwater">freshwater</a>, flowing towa', 'just detail', '<p>just detail</p>', NULL, 1, '572b479e56d84', '', '1462454174f5.jpg', NULL),
(20, 'About Bitm', 'Demo_again', 17, 'WELCOME\r\nTo address the skill gap of HR in the industry, BASIS started its own training activities in 2007. Later in 2012, BASIS institutionalized its training activities and set up BASIS Institute of Technology &amp; Management (BITM) with the support of World Bank. BITM was established with a vision to be a world-class IT institute in Bangladesh for the purpose of enhancing the competitiveness of the IT Sector in Bangladesh by creating a pool of qualified IT professionals and quality certified IT companie', '<h2 class="event_title">WELCOME</h2>\r\n<p><span style="color: #808080;">To address the skill gap of HR in the industry, BASIS started its own training activities in <strong>2007</strong>. Later in 2012, BASIS institutionalized its training activities and s', 'WELCOME\r\nTo address the skill gap of HR in the industry, BASIS started its own training activities in 2007. Later in 2012, BASIS institutionalized its training activities and set up BASIS Institute of Technology &amp; Management (BITM) with the support of World Bank. BITM was established with a vision to be a world-class IT institute in Bangladesh for the purpose of enhancing the competitiveness of the IT Sector in Bangladesh by creating a pool of qualified IT professionals and quality certified IT companie', '<h2 class="event_title">WELCOME</h2>\r\n<p><span style="color: #808080;">To address the skill gap of HR in the industry, BASIS started its own training activities in <strong>2007</strong>. Later in 2012, BASIS institutionalized its training activities and set up <strong>BASIS Institute of Technology &amp; Management (BITM)</strong> with the support of World Bank. BITM was established with a vision to be a world-class IT institute in Bangladesh for the purpose of enhancing the competitiveness of the IT Sector ', NULL, 1, '572b489f7fd20', '', '1462454431bitm_logo.png', NULL),
(21, 'Add for test', 'test', 17, 'Something for test', '<h1><strong>Something for test</strong></h1>', 'Something for test', '<p>Something for test<u></u><strike></strike></p>', NULL, 0, '572b6bb935895', '', '1462463417f3.png', NULL),
(22, 'demoooo', 'demoooo', 17, 'gfbbbbbbbbbbbbr', '<p>gfbbbbbbbbbbbbr</p>', 'zcsdgveeeeeeeeeeee', '<p>zcsdgveeeeeeeeeeee</p>', NULL, 0, '572b6f2f3f133', '', '1462464303aiub.png', 0),
(23, 'c vgrr', 'dvgewv dseg', 17, 'dgveeeeeee x', '<p>dgveeeeeee x</p>', 'csefrtqe4rqerr', '<p>csefrtqe4rqerr</p>', NULL, 0, '572b6fd680b57', '', '1462464470f2.png', 0),
(24, 'xc vsdrcf xadc', 'asfcxaece', 17, 'ascfeewfc', '<p>ascfeewfc</p>', 'avgfrsdvgzadsv', '<p>avgfrsdvgzadsv</p>', NULL, 0, '572b709d79f62', '', '1462464669f1.png', 0),
(25, 'frgbvrwgv', 'dgtrvcg', 17, 'rsgydtaewft', '<p>rsgydtaewft</p>', 'dsegtfwetfv', '<p>dsegtfwetfv</p>', NULL, 0, '572b70cd5999a', '', '1462464717aiub.png', 1462464717),
(26, 'zxvcfaec', 'swcfwafc', 17, 'scfwsdf', '<p>scfwsdf</p>', 'azcdsacfasf', '<p>azcdsacfasf</p>', NULL, 0, '572b710cd6612', '', '1462464780f5.jpg', 1462464780),
(32, 'fscsz scwa', 'asfwc zcswc ', 19, 'sacfwc zsw', 'sacfwc zsw', 'fcsc ccs ', 'fcsc ccs ', NULL, 0, '572cf825eb859', '', '1462564901aiub.png', 1462564901),
(33, 'cfAWDWX', 'CWAQCFC', 19, 'CXFEWCX CEF', 'CXFEWCX CEF', 'ASEFRCSWDC', 'ASEFRCSWDC', NULL, 0, '572cf8a974f1e', '', '1462565033f1.png', 1462565033),
(34, 'dv', 'sc sf cef ', 19, 'scfs qecfxfec', '<p>scfs qecfxfec</p>', 'vdegxaz dvegw', '<p>vdegxaz dvegw</p>', NULL, 0, '572cf9b27c694', '', '1462565298aiub.png', 1462565298),
(35, ' etb eth', 'fbr35hyb b53', 19, ' br35hybfr5h3y', ' br35hybfr5h3y', 'fr3b5hfr5h3y', 'fr3b5hfr5h3y', NULL, 0, '572d94be013e5', '', '1462604989bitm_pic.jpg', 1462604989),
(36, 'How Do You Debate Mr. Trump?', 'By THE EDITORIAL BOARD ', 19, 'Clinton vs. Trump could be the ugliest presidential contest of modern times. But in that mess there may still be an opportunity for constructive dialogue.', '<p>Clinton vs. Trump could be the ugliest presidential contest of modern times. But in that mess there may still be an opportunity for constructive dialogue.<u></u><strike></strike></p>', '', '<p><a href=', NULL, 1, '572ddbd5050e3', 'http://www.nytimes.com/', '1462623188site.jpg', 1462623189);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `publication_status` int(11) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `parent_id`, `publication_status`, `deleted_at`) VALUES
(1, 'International', 0, 0, NULL),
(2, 'National', 0, 0, NULL),
(3, 'News', 2, 1, NULL),
(4, 'Sports', 3, 1, NULL),
(5, 'Football', 0, 0, NULL),
(6, 'Demo', 0, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `uniqid` varchar(30) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  `parent_menu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `url`, `uniqid`, `deleted_at`, `parent_menu`) VALUES
(1, 'Home', 'http://www.gmail.com', '57275655f2289', NULL, NULL),
(2, 'Google', 'http://www.google.com', '5727566359dc9', NULL, NULL),
(3, 'Contact_Us', 'http://www.facebook.com', '57275681dcb34', NULL, NULL),
(5, 'National', 'http://www.facebook.com', '5728a7b0f1cdd', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `image` varchar(30) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  `mobile_no` varchar(30) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `firstname`, `lastname`, `gender`, `country`, `image`, `deleted_at`, `mobile_no`, `address`) VALUES
(2, 15, 'fffffffff', 'dwvgrr3 ', 'Male', 'bangladesh', '1462616033', NULL, '01235802', ' vdgtb');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_active` varchar(11) NOT NULL,
  `is_admin` varchar(11) NOT NULL,
  `forgotpass` varchar(30) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  `uniqid` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `is_active`, `is_admin`, `forgotpass`, `deleted_at`, `uniqid`) VALUES
(15, 'test', 't@gmail.com', 'e358efa489f58062f10dd7316b65649e', '1', '', '', NULL, '5727a7f4ddb3e'),
(17, 'Admin', 'admin@gmail.com', '0cc175b9c0f1b6a831c399e269772661', '1', '1', '', NULL, '5727a8a3bbc40'),
(19, 'user', 'uu@gmail.com', '0cc175b9c0f1b6a831c399e269772661', '1', '', '57290f21b2820', NULL, NULL),
(20, 'newentry', 'new@gmail.com', '202cb962ac59075b964b07152d234b70', '1', '', '', NULL, NULL),
(21, 'newu', 'newu@gmail.com', '0cc175b9c0f1b6a831c399e269772661', '1', '', '', NULL, '572d88c7b1395'),
(22, 'fortesting', 'testing@gmail.com', '0cc175b9c0f1b6a831c399e269772661', '', '', '', NULL, '572db49abc71b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
