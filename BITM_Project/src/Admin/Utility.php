<?php

namespace App\Admin;

class Utility{
    public static function d($data=''){
        echo "<pre>";
        echo var_dump($data);
        echo '</pre>';
    }
    
    public static function dd($data='') {
        echo "<pre>";
        echo var_dump($data);
        echo '</pre>';
        die();
    }
    
    public static function redirect() {
        header ('Location:index.php');
    }
    public static function redirect2() {
        header ('Location:../../views/User_info/userpage.php');
    }
    public static function redirect3() {
        header ('Location:user_article_index.php');
    }
}