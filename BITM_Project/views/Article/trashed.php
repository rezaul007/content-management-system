<?php
ini_set("error reporting",E_ALL & ~E_DEPRECATED);
include_once '../../vendor/autoload.php';

use App\Admin\Article;
session_start();
use App\User\Info;
$id=Info::info();
$obj=new Article();
$trashed=$obj->trashed();
//var_dump($trashed);
//die();

if(Info::info()!=NULL){
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="trashed.css">
        <title>Trashed Categories</title>
</head>
<body>
<!--    <div class="header">
        <img src="../../../index_images/email.jpg"  />
 </div> -->
<div class="menubar">
    <ul>
        
    
        <li><a href="create.php">Go to create page</a></li>
        <li><a href="index.php" aling="right">Go To Index</a></li>
         <li><a href="../User_info/home_page.php">Home</a></li>
        <li><a href="../User_info/logout.php" align="right">Logout</a></li>
    
    </ul>
    
</div>
     <br>
    
    <h3 align="center">Trashed data</h3>
<table border="1">
     <tr>
         <th>SL</th>
    <th>ID</th>
    
    <th>Title</th>
    <th>Sub_Title</th>
    <th>Summary</th>
    <th>Detail</th>
    <th>Publication_Status</th>
    <th>URL</th>
    <th>Image</th>
    <th>Action</th>
   </tr>
  <?php 
  $serial=0;
  foreach($trashed as $trasheds){
//      var_dump($trasheds);
//      die();
      $serial++;
      ?>
 
  <tr>
    <td><?php echo $serial ?> </td>
    <td><?php echo $trasheds['id']?> </td>
    <td><?php echo $trasheds['title']?> </td>
    <td><?php echo $trasheds['sub_title']?> </td>
    <td><?php echo $trasheds['summary']?> </td>
    <td><?php echo $trasheds['detaile']?> </td>
    <td><?php echo $trasheds['publish_status']?> </td>
    <td><?php echo $trasheds['url']?> </td>
    <td><img src="../../images/<?php echo $trasheds['image']?>" alt="Image" height="100" width="100"></td>
   	
    <td><a href="recover.php?uniqid=<?php echo $trasheds['uniqid']; ?>">Recover</a> | 
        <a href="delete.php?uniqid=<?php echo $trasheds['uniqid']; ?>">Delete</a></td>
  </tr>
  <?php } ?>
 
</table>

</body>
</html>
<?php
}
else {
header('Location:../User_info/index.php');
 }
?>