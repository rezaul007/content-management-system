<?php
ini_set("error reporting",E_ALL & ~E_DEPRECATED);
include_once '../../vendor/autoload.php';

use App\Admin\Category;
session_start();
use App\User\Info;
$id=Info::info();
$obj=new Category();
$trashed=$obj->trashed();
//var_dump($trashed);
//die();

if(Info::info()!=NULL){
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="trashed.css">
        <title>Trashed Category </title>
</head>
<body>
<!--    <div class="header">
        <img src="../../../index_images/email.jpg"  />
 </div> -->
<div class="menubar">
    <ul>
        
    
        <li><a href="create.php">Go to create page</a></li>
        <li><a href="index.php" aling="right">Go To Index</a></li>
         <li><a href="../User_info/home_page.php">Home</a></li>
        <li><a href="../User_info/logout.php" align="right">Logout</a></li>
    
    </ul>
    
</div>
     <br>
    
    <h3 align="center">Trashed data</h3>
<table border="1">
     <tr>
         <th>SL</th>
    <th>ID</th>
    
    <th>Title</th>
    <th>Parent_ID</th>
    <th>Publication_Status</th>
    <th>Action</th>
   </tr>
  <?php 
  $serial=0;
  foreach($trashed as $trasheds){
//      var_dump($trasheds);
//      die();
      $serial++;
      ?>
 
  <tr>
    <td><?php echo $serial ?> </td>
    <td><?php echo $trasheds['id']?> </td>
    <td><?php echo $trasheds['title']?> </td>
    <td><?php echo $trasheds['parent_id']?> </td>
    <td><?php echo $trasheds['publication_status']?> </td>
   	
    <td><a href="recover.php?id=<?php echo $trasheds['id']; ?>">Recover</a> | 
        <a href="delete.php?id=<?php echo $trasheds['id']; ?>">Delete</a></td>
  </tr>
  <?php } ?>
 
</table>

</body>
</html>
<?php
}
else {
header('Location:../User_info/index.php');
 }
?>