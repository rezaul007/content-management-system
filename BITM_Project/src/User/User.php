<?php

namespace App\User;
use App\User\Utility ;
use App\User\Message;
use App\User\Info;
use App\User\Info2;
use PDO;
//session_start();
class User{
    public $id="";
    public $email="";
    public $user_name="";
    public $is_active="";
    public $password='';
    public $oldpassword='';
    public $newpassword='';
    public $confirmpass='';
    public $forgotpass='';
    public $conn;
    public $user='root';
    public $pass='';
  
    public $user_id="";
    public $firstname="";
    public $lastname="";
    public $mobile_no="";
    public $address="";
    public $image="";
    public $country="";
    public $gender="";
    public $uniqid="";
    //public $image="";

    
    public function __construct() {
        $this->conn = new PDO('mysql:host=localhost;dbname=owncms', $this->user, $this->pass);
        
    }
    public function prepare($data=array()){
        if(is_array($data) && array_key_exists('email', $data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('id',$data) && !empty($data['id'])){
            $this->id=$data['id'];
        }
        
        if(is_array($data) && array_key_exists('username',$data)){
             $this->user_name=$data['username'];
        }
        if(is_array($data) && array_key_exists('user_name',$data)){
             $this->user_name=$data['user_name'];
        }
        if(is_array($data) && array_key_exists('firstname',$data)){
             $this->firstname=$data['firstname'];
        }
        if(is_array($data) && array_key_exists('lastname',$data)){
             $this->lastname=$data['lastname'];
        }
        if(is_array($data) && array_key_exists('gender',$data)){
             $this->gender=$data['gender'];
        }
        if(is_array($data) && array_key_exists('mobile_no',$data)){
             $this->mobile_no=$data['mobile_no'];
        }
        if(is_array($data) && array_key_exists('address',$data)){
             $this->address=$data['address'];
        }
        if(is_array($data) && array_key_exists('country',$data)){
             $this->country=$data['country'];
        }
        if(is_array($data) && array_key_exists('confirmpass',$data)){
             $this->confirmpass=$data['confirmpass'];
        }
         if(is_array($data) && array_key_exists('password',$data)){
             if(!empty($_POST['password'])){//after update the password may not change
             $this->password=md5($data['password']);}
        }
        if(is_array($data) && array_key_exists('oldpassword',$data)){
             if(!empty($_POST['oldpassword'])){//after update the password may not change
             $this->oldpassword=md5($data['oldpassword']);}
        }
        if(is_array($data) && array_key_exists('newpassword',$data)){
             if(!empty($_POST['newpassword'])){//after update the password may not change
             $this->newpassword=md5($data['newpassword']);}
        }
//       
        if(array_key_exists('user_id',$data) && !empty($data['user_id'])){
            $this->user_id=$data['user_id'];
        }
        if(array_key_exists('image',$data) && !empty($data['image'])){
            $this->image=$data['image'];
        }
        
       return $this;
//       var_dump($this);
//       die();

    }
    public function login() {
//        var_dump($Data);
//        die();
        $allData = array();
        $query = "SELECT * FROM `owncms`.`users` WHERE `users`.`email`=:email  AND `users`.`password`=:password AND `users`.`is_active`=1";
        $result = $this->conn->prepare( $query);
        $result->execute(array(':email'=>$this->email,':password'=>$this->password));
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
//        var_dump($allData);
//        die();

        //Message::message($username);
        
       if ($allData!=true) {
           Message::message('Please Give Valid UserName Or Password'); 
           Utility::redirect6();
          
            }
        else {
                foreach ($allData as $user) {
                    if($user['is_admin']==1){
                    Message::message('Welcome '.$user['username']) ;
                    Info::info($user['id']);
                    Info2::info2($user['is_admin']);
                    Utility::redirect2();
                    }
                    else{
                        Message::message('Welcome '.$user['username']) ;
                        Info::info($user['id']);
                        Info2::info2($user['is_admin']);
                        Utility::redirect2();
                    }
            //die();
                }
                //Utility::redirect2();
        }
        //return $row;
    }
    public function show_categories() {
//        var_dump($data);
//        die();
       
        $allData = array();
        $query = "SELECT * FROM `owncms`.`menus` WHERE `deleted_at` IS NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
        
   
    
    }
//    
     public function store() {
         $uid=  uniqid();
         $this->uniqid=$uid;
             if (!empty($this->user_name) && !empty($this->email) && !empty($this->password) ) {
            $query = "INSERT INTO `owncms`.`users` (`username`,`email`,`password`,`uniqid`) VALUES (:user_name,:email,:password,:uniqid)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':user_name'=>$this->user_name,':email'=>$this->email,':password'=>$this->password,':uniqid'=>$this->uniqid));
//            var_dump($result);
//         die();
            if ($result) {
             //Message::message('Registration successful');
              Utility::redirect();
            }
        } else {
          //Utility::redirect();
        }
        
    }
//    public function store2() {
//         
//             //if (!empty($this->org_name) && !empty($this->org_add) && !empty($this->phone) && !empty($this->summary) ) {
//            $query = "INSERT INTO `miniproject`.`user_profile` (`first_name`,`last_name`,`per_phone`,`org_name`, `org_add`,`phone`,`summary`,`user_id`) VALUES (:first_name,:last_name,:per_phone,:org_name,:org_add,:phone,:summary,:id)";
////            var_dump($query);
////            die();
//            $result = $this->conn->prepare($query);
//            $result->execute(array(':first_name'=>$this->first_name,':last_name'=>$this->last_name,':per_phone'=>$this->per_phone,':org_name'=>$this->org_name,':org_add'=>$this->org_add,':phone'=>$this->phone,':summary'=>$this->summary,':id'=>$this->user_id));
//
//            if ($result) {
//             Message::message("Data has been stored successfully");
//              Utility::redirect2();
//            }
//        //}
//        else {
//            Message::message("Data hasnot been stored successfully");
//          Utility::redirect2();
//        }
//        
//    }
//    
//     public function edit() {
////         var_dump($_GET);
////         die();
//         $query = "SELECT * FROM `miniproject`.`user_profile` WHERE `id`=:id ";
//       
//        $result = $this->conn->prepare( $query);
//        $result->execute(array(':id'=>$this->id));
//        $row = $result->fetch(PDO::FETCH_ASSOC);
//        return $row;
//       
//    }
//    
     public function select() {
//         var_dump($_POST);
//         die();
          
        $query = "SELECT * FROM `owncms`.`users` WHERE `users`.`forgotpass` = :confirmpass";
        $result = $this->conn->prepare($query);
        $result->execute(array(':confirmpass'=>$this->confirmpass));
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
//        var_dump($result);
//        die();
         if($allData){
             
             foreach ($allData as $user) {
                    if($user['is_admin']==1){
                    Message::message('Welcome '.$user['username']) ;
                    Info::info($user['id']);
                    Info2::info2($user['is_admin']);
                    Utility::redirect3();
                    }
                    else{
                        Message::message('Welcome '.$user['username']) ;
                        Info::info($user['id']);
                        Info2::info2($user['is_admin']);
                        Utility::redirect3();
                    }
            
                }
             //header('Location:http://localhost/BITM_Project/views/User_info/home_page.php');
         }
         else{
             Utility::redirect4();
             //header('Location:http://localhost/BITM_Project/views/User_info/index.php');
         }

 
    }
    public function resetpass() {
//         var_dump($this);
//         die();
        $cid=Info::info();
//        var_dump($cid);
//        die();
         if (!empty($this->newpassword)&&!empty($this->oldpassword)) {  
         $query = "UPDATE `owncms`.`users` SET `password`=:newpassword WHERE `users`.`password`=:oldpassword";// 
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':oldpassword'=>$this->oldpassword,':newpassword'=>$this->newpassword));
        //$allData=$result->fetchAll(PDO::FETCH_ASSOC);
        //return $allData;
//        var_dump($allData);
//        die();
         if($result){
             Message::message("Password has been updated successfully");
             Utility::redirect2();
         }
         else{
             Message::message("Password Didn't Match");
             Utility::redirect2();
         }
      }
   
      elseif(empty($this->newpassword)&& empty($this->oldpassword)){
             Utility::redirect2();
         }
//      elseif(empty($this->oldpassword)){
//             Utility::redirect2();
//         }
//      elseif(empty($this->newpassword)){
//             Utility::redirect2();
//         }
     else {
             Utility::redirect2();
            }
 }
 
    
//    
//     public function delete(){
////         var_dump($data);
////         die();
//       $query = "DELETE FROM `miniproject`.`user_profile` WHERE `user_profile`.`id` =:id" ;
//        //echo $query;
//        $result = $this->conn->prepare($query);
//        $result->execute(array(':id'=>$this->id));
//        //var_dump($result);
//        //die();
//        if($result){
//            Message::message("Data has bee deleted successfully");
//            Utility::redirect2();
//        }
//        else{
//            Utility::redirect2();
//        }
//         
//         
//     }
    
//     public function show() {
//         
//         $query = "SELECT * FROM `atomicprojectb16`.`email` WHERE `id`=:id";
//       
//        $result = $this->conn->prepare( $query);
//        $result->execute(array(':id'=>$this->id));
//        $row = $result->fetch(PDO::FETCH_ASSOC);
//        return $row;
//    }
    public function forgot() {
        $this->forgotpass=  uniqid();
        //var_dump($fid);
        //var_dump($_POST);
        //die();
        $query = "UPDATE `owncms`.`users` SET `forgotpass`=:forgotpass WHERE `users`.`username` =:username";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':username'=>$this->user_name,':forgotpass'=>$this->forgotpass));
       
         if($result){
         Message::message("Message has been sent");
         Info::info($this->user_name);
         header('Location:phpmailer2.php');
         //header('Location:phpmailer2.php');
       }
    }
    
    public function index2($data='') {//sinlg row mail
//        var_dump($data);
//        die();
        $query = "SELECT * FROM `owncms`.`users` WHERE `users`.`username` = :username";
        
        $result = $this->conn->prepare($query);
        
        $result->execute(array(':username'=>$data));
      
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
//        var_dump($allData);
//        die();
    }
    //}
//      
//       
//        else {
//           Utility::redirect();
//        }
//    }
//    public function trashed() {
//        $allData = array();
//        $query = "SELECT * FROM `atomicprojectb16`.`email` WHERE `deleted_at` IS NOT NULL" ;
//        $result = $this->conn->query( $query);
//        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
//        return $allData;
//    }
//    public function recover() {
//        
//        $query = "UPDATE `atomicprojectb16`.`email` SET `deleted_at`=:NULL WHERE `email`.`id` = :id";
//        //echo $query;
//        $result = $this->conn->prepare($query);
//        $result->execute(array(':id'=>$this->id,':NULL'=>NULL));
//         if($result){
//         Message::message("Data has been recovered successfully");
//         Utility::redirect();
//       }
//      
//       
//        else {
//           Utility::redirect();
//        }
//    }
    public function logout() {
        session_destroy();
        
       session_unset(Info::info('id'));
        Utility::redirect();
    }
//    public function account($data='') {
//        //var_dump($data);
//        //die();
//       
//        $allData = array();
//        $query = "SELECT * FROM `miniproject`.`user_profile` WHERE `user_profile`.`user_id`=:id ";//AND `user`.`password`=:password"
//        $result = $this->conn->prepare( $query);
//        $result->execute(array(':id'=>$data));
//        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
//        //var_dump($allData);
//        //die();
//        return $allData;
//        
//   
//    
//    }
    public function privacy($data='') {
        //var_dump($data);
        //die();
       
        $allData = array();
        $query = "SELECT * FROM `owncms`.`users` WHERE `users`.`id`=:id ";//AND `user`.`password`=:password"
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$data));
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        //var_dump($allData);
        //die();
        return $allData;
      
    }
    public function update2() {
//         var_dump($_POST);
//         die();
         if (!empty($this->user_name) && !empty($this->email) && !empty($this->password) ) {  
         $query = "UPDATE `owncms`.`users` SET `email`=:email,`username` =:user_name,`password`=:password WHERE `users`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':email'=>$this->email,':user_name'=>$this->user_name,':password'=>$this->password));

         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect2();
         }
         else{
             Utility::redirect2();
         }
         }
    elseif(empty($this->password) && !empty($this->user_name) && !empty($this->email)) {
     $query = "UPDATE `owncms`.`users` SET `email`=:email,`username` =:user_name WHERE `users`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':email'=>$this->email,':user_name'=>$this->user_name));

         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect2();
         }
         else{
             Utility::redirect2();
         }
 }
 else {
     Utility::redirect2();
 }
 
    }
    

    public function profile($data='') {
//        var_dump($data);
//        die();
     
        $allData = array();
        $query = "SELECT * FROM `owncms`.`users` LEFT JOIN `profiles` on `users`.`id`= `profiles`.`user_id`  WHERE `users`.`id`=:id";
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$data));
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
//        var_dump($allData);
//        die();
        return $allData;
        
     }
     public function check_profile($data=''){
         $allData = array();
        $query = "SELECT * FROM `owncms`.`profiles` WHERE `profiles`.`user_id`=:id";
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$data));
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
//        var_dump($allData);
//        die();
        return $allData;
     }
     public function profile_update() {
//        var_dump($data);
//        die();
        $uid=uniqid();
         $this->uniqid=$uid;
//         var_dump($uid);
//         die();
            
        $allData = array();
        if(!empty($this->image)){
        $query = "UPDATE `owncms`.`profiles` SET `firstname`=:firstname,`lastname`=:lastname,`gender`=:gender,`mobile_no`=:mobile_no,`address`=:address,`country`=:country,`image`=:image WHERE `profiles`.`user_id`=:id";
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->user_id,':firstname'=>$this->firstname,':lastname'=>$this->lastname,':gender'=>$this->gender,':mobile_no'=>$this->mobile_no,':address'=>$this->address,':country'=>$this->country,':image'=>$this->image));
//        var_dump($result);
//        die();
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect5();
         }
         else{
             Utility::redirect5();
         }
        }
 else {
        $query = "UPDATE `owncms`.`profiles` SET `firstname`=:firstname,`lastname`=:lastname,`gender=:gender,`mobile_no`=:mobile_no,`address`=:address,`country`=:country WHERE `profiles`.`user_id`=:id";
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->user_id,':firstname'=>$this->firstname,':lastname'=>$this->lastname,':gender'=>$this->gender,':mobile_no'=>$this->mobile_no,':address'=>$this->address,':country'=>$this->country));
//        var_dump($result);
//        die();
         if($result){
             Message::message("Data updated successfully");
             Utility::redirect5();
         }
         else{
             Utility::redirect5();
         }
 }
        
     }
public function profile_insert() {//INSERT INTO `profiles` (`id`, `firstname`, `lastname`, `mobile_no`, `address`, `country`, `gender`, `image`, `user_id`) VALUES (NULL, 'fffffff', 'lllllllllll', '05070', ' vdgtb', 'bd', 'Male', NULL, '19'); 
         $uid=  uniqid();
         //$this->uniqid=$uid;
             //if (!empty($this->user_name) && !empty($this->email) && !empty($this->password) ) {
            $query = "INSERT INTO `owncms`.`profiles` (`firstname`,`lastname`,`gender`,`address`,`mobile_no`,`country`,`user_id`,`image`) VALUES (:firstname,:lastname,:gender,:address,:mobile_no,:country,:user_id,:image)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':firstname'=>$this->firstname,':lastname'=>$this->lastname,':gender'=>$this->gender,':address'=>$this->address,':mobile_no'=>$this->mobile_no,':country'=>$this->country,':user_id'=>$this->user_id,':image'=>$this->image));
//            var_dump($result);
//         die();
            if ($result) {
             Message::message('Update successful');
              Utility::redirect5();
            }
//        } 
        else {
          Utility::redirect5();
        }
        
    }
    public function profile_update2() {
//        var_dump($data);
//        die();
        $uid=uniqid();
         $this->uniqid=$uid;
//         var_dump($uid);
//         die();
            
        $allData = array();
        if(!empty($this->image)){
        $query = "UPDATE `owncms`.`profiles` SET `firstname`=:firstname,`lastname`=:lastname,`gender`=:gender,`mobile_no`=:mobile_no,`address`=:address,`country`=:country,`image`=:image WHERE `profiles`.`user_id`=:id";
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->user_id,':firstname'=>$this->firstname,':lastname'=>$this->lastname,':gender'=>$this->gender,':mobile_no'=>$this->mobile_no,':address'=>$this->address,':country'=>$this->country,':image'=>$this->image));
//        var_dump($result);
//        die();
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect2();
         }
         else{
             Utility::redirect2();
         }
        }
 else {
        $query = "UPDATE `owncms`.`profiles` SET `firstname`=:firstname,`lastname`=:lastname,`gender=:gender,`mobile_no`=:mobile_no,`address`=:address,`country`=:country WHERE `profiles`.`user_id`=:id";
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->user_id,':firstname'=>$this->firstname,':lastname'=>$this->lastname,':gender'=>$this->gender,':mobile_no'=>$this->mobile_no,':address'=>$this->address,':country'=>$this->country));
//        var_dump($result);
//        die();
         if($result){
             Message::message("Data updated successfully");
             Utility::redirect2();
         }
         else{
             Utility::redirect2();
         }
 }
        
     }
public function profile_insert2() {//INSERT INTO `profiles` (`id`, `firstname`, `lastname`, `mobile_no`, `address`, `country`, `gender`, `image`, `user_id`) VALUES (NULL, 'fffffff', 'lllllllllll', '05070', ' vdgtb', 'bd', 'Male', NULL, '19'); 
         $uid=  uniqid();
         //$this->uniqid=$uid;
             //if (!empty($this->user_name) && !empty($this->email) && !empty($this->password) ) {
            $query = "INSERT INTO `owncms`.`profiles` (`firstname`,`lastname`,`gender`,`address`,`mobile_no`,`country`,`user_id`,`image`) VALUES (:firstname,:lastname,:gender,:address,:mobile_no,:country,:user_id,:image)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':firstname'=>$this->firstname,':lastname'=>$this->lastname,':gender'=>$this->gender,':address'=>$this->address,':mobile_no'=>$this->mobile_no,':country'=>$this->country,':user_id'=>$this->user_id,':image'=>$this->image));
//            var_dump($result);
//         die();
            if ($result) {
             Message::message('Update successful');
              Utility::redirect2();
            }
//        } 
        else {
          Utility::redirect2();
        }
        
    }
}

