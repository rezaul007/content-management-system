<?php

namespace App\Admin;
use App\Admin\Utility;
use App\Admin\Message;
use PDO;
class Category{
    public $id="";
    
    public $search="";
    public $title="";
    public $parent_id="";
    public $publication_status="";
    public $conn;
    public $user='root';
    public $pass='';
    public $deleted_at=""; 
    
    public function __construct() {
        $this->conn = new PDO('mysql:host=localhost;dbname=owncms', $this->user, $this->pass);
        
    }
    public function prepare($data=array()){
        if(is_array($data) && array_key_exists('title', $data)){
            $this->title=$data['title'];
        }
        if(is_array($data) && array_key_exists('publication_status', $data)){
            $this->publication_status=$data['publication_status'];
        }
        if(array_key_exists('id',$data) && !empty($data['id'])){
            $this->id=$data['id'];
        }
//        if(array_key_exists('uniqid',$data) && !empty($data['uniqid'])){
//            $this->uniqid=$data['uniqid'];
//        }
        
        if(array_key_exists('parent_id',$data) && !empty($data['parent_id'])){
            $this->parent_id=$data['parent_id'];
        }
//        if(is_array($data) && array_key_exists('parent_id',$data)){
//             $this->parent_id=$data['parent_id'];
//        }
       if(array_key_exists('deleted_at', $data) && !empty($data['deleted_at'])){
           $this->deleted_at=$data['deleted_at'];
       }
       if (array_key_exists('search', $data) && !empty($data['search'])) {
            $this->search = $data['search'];
        }
      return $this;
//       var_dump($this);
//       die();
    }
    public function index() {
//        var_dump($_GET);
//        die();
        $allData = array();
        $whereClause=" 1=1 ";
        
        if(!empty($this->search)){
            $whereClause.="AND `title` LIKE '%{$this->search}%'";

        }

        
        $query = "SELECT * FROM `owncms`.`category` WHERE `deleted_at` IS NULL AND".$whereClause;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
//        var_dump($allData);
//        die();
    }
    public function index2() {//sinlg row mail
        $query = "SELECT * FROM `owncms`.`category` WHERE `category`.`id` = :id";
        
        $result = $this->conn->prepare($query);
        
        $result->execute(array(':id'=>$this->id));
      
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        
        return $allData;
    }
    
    
     public function store() {
//         var_dump($_POST);
//         die();
//         $id=$_POST['id'];
//         $uniq_id=  uniqid($id);
             if (!empty($this->title)) {
            $query = "INSERT INTO `owncms`.`category` (`title`, `publication_status`,`parent_id`) VALUES (:title,:publication_status,:parent_id)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':title'=>$this->title,':publication_status'=>$this->publication_status,':parent_id'=>$this->parent_id));

            if ($result) {
                Message::message("Data has been stored successfully");
                Utility::redirect();
            }
            else {
            Utility::redirect();
        }
             }
        
        else {
            Utility::redirect();
        }
            
    }
    
     public function edit() {
         $query = "SELECT * FROM `owncms`.`category` WHERE `category`.`id`=:id ";
       
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$this->id));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
       
    }
    
     public function update() {
        
        if(!empty($this->title))  {
         $query = "UPDATE `owncms`.`category` SET `title`=:title,`publication_status` =:publication_status,`parent_id` =:parent_id WHERE `category`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':title'=>$this->title,':publication_status'=>$this->publication_status,':parent_id'=>$this->parent_id));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
        }
        elseif(!empty($this->title))  {
         $query = "UPDATE `owncms`.`category` SET `title`=:title WHERE `category`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':title'=>$this->title));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
        }
        elseif(!empty($this->publication_status))  {
         $query = "UPDATE `owncms`.`category` SET `publication_status` =:publication_status WHERE `category`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':publication_status'=>$this->publication_status));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
        }
     elseif(empty($this->title)&& empty($this->publication_status)) {
        Utility::redirect();
        }
 else {
     Utility::redirect();
 }

 
    }
    
     public function delete($data="") {
       $query = "DELETE FROM `owncms`.`category` WHERE `category`.`id` =:id" ;
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id));
        //var_dump($result);
        //die();
        if($result){
            Message::message("Data has bee deleted successfully");
            Utility::redirect();
        }
        else{
            Utility::redirect();
        }
         
         
     }
    
     public function show() {
         
         $query = "SELECT * FROM `owncms`.`category` WHERE `id`=:id";
       
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$this->id));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    public function homepage_show() {
         
         $query = "SELECT * FROM `owncms`.`category` WHERE `deleted_at` IS NULL";
       $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
    }
    public function trash() {
        $this->deleted_at=  time();
        $query = "UPDATE `owncms`.`category` SET `deleted_at`=:deleted_at WHERE `category`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':deleted_at'=>$this->deleted_at));
         if($result){
         Message::message("Data has been trashed successfully");
         Utility::redirect();
       }
      
       
        else {
           Utility::redirect();
        }
    }
    public function trashed() {
        $allData = array();
        $query = "SELECT * FROM `owncms`.`category` WHERE `deleted_at` IS NOT NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
    }
    public function recover() {
//        var_dump($_GET);
//        die();
        $query = "UPDATE `owncms`.`category` SET `deleted_at`=:deleted_at WHERE `category`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':deleted_at'=>NULL));
         if($result){
         Message::message("Data has been recovered successfully");
         Utility::redirect();
       }
      
       
        else {
           Utility::redirect();
        }
    }
    public function getALLtitle(){
        $allData = array();
        $query = "SELECT `title` FROM `owncms`.`category` WHERE `deleted_at` IS NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;

    }
    public function logout() {
        session_destroy();
        
       session_unset(Info::info('id'));
        Utility::redirect();
    }
}

