<?php
ini_set("error reporting",E_ALL & ~E_DEPRECATED);
include_once '../../vendor/autoload.php';

use App\Admin\Users;
session_start();
use App\User\Info;
$id=Info::info();
$obj=new Users();
$trashed=$obj->trashed();
//var_dump($trashed);
//die();

if(Info::info()!=NULL){
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="trashed.css">
        <title>Trashed Users </title>
</head>
<body>
<!--    <div class="header">
        <img src="../../../index_images/email.jpg"  />
 </div> -->
<div class="menubar">
    <ul>
        
    
        <li><a href="create.php">Go to create page</a></li>
        <li><a href="index.php" aling="right">Go To Index</a></li>
         <li><a href="../User_info/home_page.php">Home</a></li>
        <li><a href="../User_info/logout.php" align="right">Logout</a></li>
    
    </ul>
    
</div>
     <br>
    
    <h3 align="center">Trashed data</h3>
<table border="1">
     <tr>
         <th>SL</th>
    <th>ID</th>
    
    <th>User_Name</th>
    <th>Is_Admin</th>
    <th>Is_Active</th>
    <th>Email Address</th>
    <th>Action</th>
   </tr>
  <?php 
  $serial=0;
  foreach($trashed as $trasheds){
//      var_dump($trasheds);
//      die();
      $serial++;
      ?>
 
  <tr>
    <td><?php echo $serial ?> </td>
    <td><?php echo $trasheds['id']?> </td>
    <td><?php echo $trasheds['username']?> </td>
    <td><?php echo $trasheds['is_admin']?> </td>
    <td><?php echo $trasheds['is_active']?> </td>
    <td><?php echo $trasheds['email']?> </td>
   	
    <td><a href="recover.php?uniqid=<?php echo $trasheds['uniqid']; ?>">Recover</a> | 
        <a href="delete.php?uniqid=<?php echo $trasheds['uniqid']; ?>">Delete</a></td>
  </tr>
  <?php } ?>
 
</table>

</body>
</html>
<?php
}
else {
header('Location:../User_info/index.php');
 }
?>