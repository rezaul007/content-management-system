<?php

namespace App\Admin;
use App\Admin\Utility;
use App\Admin\Message;
use PDO;
class Article{
    public $id="";
    public $title="";
    public $user_id="";
    public $sub_title="";
    public $summary="";
    public $html_summary="";
    public $detaile="";
    public $html_detaile="";
    public $publication_status="";
    public $image="";
    public $url="";
    public $uniqid="";
    public $search="";
    
    public $conn;
    public $user='root';
    public $pass='';
    public $deleted_at=""; 
    public $created_at="";
    public $block="";
    
    public function __construct() {
        $this->conn = new PDO('mysql:host=localhost;dbname=owncms', $this->user, $this->pass);
        
    }
    public function prepare($data=array()){
        if(is_array($data) && array_key_exists('title', $data)){
            $this->title=$data['title'];
        }
        if(array_key_exists('id',$data) && !empty($data['id'])){
            $this->id=$data['id'];
        }
        if(array_key_exists('uniqid',$data) && !empty($data['uniqid'])){
            $this->uniqid=$data['uniqid'];
        }
        if(array_key_exists('user_id',$data) && !empty($data['user_id'])){
            $this->user_id=$data['user_id'];
        }
        
        if(is_array($data) && array_key_exists('sub_title',$data)){
             $this->sub_title=$data['sub_title'];
        }
//        if(is_array($data) && array_key_exists('parent_id',$data)){
//             $this->parent_id=$data['parent_id'];
//        }
       if(array_key_exists('deleted_at', $data) && !empty($data['deleted_at'])){
           $this->deleted_at=$data['deleted_at'];
       }
       if (array_key_exists('summary', $data) && !empty($data['summary'])) {
            $this->summary=$data['summary'];
        }
        if (array_key_exists('html_summary', $data) && !empty($data['html_summary'])) {
            $this->html_summary=$data['html_summary'];
        }
        if (array_key_exists('html_detaile', $data) && !empty($data['html_detaile'])) {
            $this->html_detaile=$data['html_detaile'];
        }
        if (array_key_exists('detaile', $data) && !empty($data['detaile'])) {
            $this->detaile=$data['detaile'];
        }
        if (array_key_exists('publish_status', $data) && !empty($data['publish_status'])) {
            $this->publication_status=$data['publish_status'];
        }
        if (array_key_exists('url', $data) && !empty($data['url'])) {
            $this->url=$data['url'];
        }
        if (array_key_exists('image', $data) && !empty($data['image'])) {
            $this->image=$data['image'];
        }
        if (array_key_exists('search', $data) && !empty($data['search'])) {
            $this->search=$data['search'];
        }
        if (array_key_exists('block', $data) && !empty($data['block'])) {
            $this->block=$data['block'];
        }
      return $this;
//       var_dump($this);
//       die();
    }
    public function index() {
//        var_dump($_GET);
//        die();
        $allData = array();
        $whereClause=" 1=1 ";
        
        if(!empty($this->search)){
            $whereClause.="AND `title` LIKE '%{$this->search}%'";

        }

        
        $query = "SELECT * FROM `owncms`.`article` WHERE `deleted_at` IS NULL AND".$whereClause;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
//        var_dump($allData);
//        die();
    }
    public function index2() {//sinlg row mail
        $query = "SELECT * FROM `owncms`.`article` WHERE `article`.`id` = :id";
        
        $result = $this->conn->prepare($query);
        
        $result->execute(array(':id'=>$this->id));
      
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        
        return $allData;
    }
    public function index3($data='') {//user change his article with his id
//        var_dump($_GET);
//        die();
        $allData = array();
        $whereClause=" 1=1 ";
        
        if(!empty($this->search)){
            $whereClause.="AND `title` LIKE '%{$this->search}%'";

        }

        
        $query = "SELECT * FROM `owncms`.`article` WHERE `deleted_at` IS NULL AND `article`.`user_id`=:id AND".$whereClause;
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$data));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        
        if($row){
        return $row;
        }
        else{
            Message::message("Sorry you don't have any article");
            //return $row;
            //Utility::redirect3();
        }
//        var_dump($allData);
//        die();
    }
    public function select_category() {//sinlg row mail
        $query = "SELECT * FROM `owncms`.`category` WHERE `category`.`deleted_at` IS NULL";
        
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
    }
    
    
     public function store() {
//         var_dump($_POST);
//         die();
         $string=$_POST['html_summary'];
         $string2=$_POST['html_detaile'];
         
         $str=  strip_tags($string);
//         var_dump($str);
//             die();
         $str2=  strip_tags($string2);
//         var_dump($str2);
//         die();
//         
         $id=$_POST['id'];
         $uniq_id=  uniqid($id);
         $this->created_at=  time();
//         var_dump($this->created_at);
//         die();
             if (!empty($this->title) && !empty($this->html_summary)) {
            $query = "INSERT INTO `owncms`.`article` (`title`, `sub_title`,`summary`,`html_summary`,`detaile`,`html_detaile`,`publish_status`,`url`,`image`,`uniqid`,`user_id`,`created_at`) VALUES (:title,:sub_title,:summary,:html_summary,:detaile,:html_detaile,:publish_status,:url,:image,:uniqid,:user_id,:created_at)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':title'=>$this->title,':sub_title'=>$this->sub_title,':summary'=>$str,':detaile'=>$str2,':html_summary'=>$this->html_summary,':html_detaile'=>$this->html_detaile,':publish_status'=>$this->publication_status,':url'=>$this->url,':image'=>$this->image,':uniqid'=>$uniq_id,':user_id'=>$this->user_id,':created_at'=>$this->created_at));

            if ($result) {
                Message::message("Data has been stored successfully");
                Utility::redirect();
            }
            else {
            Utility::redirect();
        }
             }
        
        else {
            Utility::redirect();
        }
            
    }
    public function store2() {
//         var_dump($_POST);
//         die();
         $string=$_POST['html_summary'];
         $string2=$_POST['html_detaile'];
         
         $str=  strip_tags($string);
//         var_dump($str);
//             die();
         $str2=  strip_tags($string2);
//         var_dump($str2);
//         die();
//         
         $id=$_POST['id'];
         $uniq_id=  uniqid($id);
         $this->created_at=  time();
//         var_dump($this->created_at);
//         die();
             if (!empty($this->title) && !empty($this->html_summary)) {
            $query = "INSERT INTO `owncms`.`article` (`title`, `sub_title`,`summary`,`html_summary`,`detaile`,`html_detaile`,`publish_status`,`url`,`image`,`uniqid`,`user_id`,`created_at`) VALUES (:title,:sub_title,:summary,:html_summary,:detaile,:html_detaile,:publish_status,:url,:image,:uniqid,:user_id,:created_at)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':title'=>$this->title,':sub_title'=>$this->sub_title,':summary'=>$str,':detaile'=>$str2,':html_summary'=>$this->html_summary,':html_detaile'=>$this->html_detaile,':publish_status'=>$this->publication_status,':url'=>$this->url,':image'=>$this->image,':uniqid'=>$uniq_id,':user_id'=>$this->user_id,':created_at'=>$this->created_at));

            if ($result) {
                Message::message("Data has been stored successfully");
                Utility::redirect();
            }
            else {
            Utility::redirect();
        }
             }
        
        else {
            Utility::redirect();
        }
            
    }
    
     public function edit() {
         $query = "SELECT * FROM `owncms`.`article` WHERE `uniqid`=:uniqid ";
       
        $result = $this->conn->prepare( $query);
        $result->execute(array(':uniqid'=>$this->uniqid));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
       
    }
    
     public function update() {
        
         $string=$_POST['html_summary'];
         $string2=$_POST['html_detaile'];
         
         $str=  strip_tags($string);
//         var_dump($str);
//             die();
         $str2=  strip_tags($string2);
         
         
        if(!empty($this->title)&& !empty($this->sub_title) && !empty($this->image))  {
         $query = "UPDATE `owncms`.`article` SET `title`=:title,`sub_title`=:sub_title,`summary`=:summary,`html_summary`=:html_summary,`publish_status`=:publish_status,`html_detaile`=:html_detaile,`detaile`=:detaile,`image`=:image  WHERE `article`.`uniqid` = :uniqid";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':uniqid'=>$this->uniqid,':title'=>$this->title,':sub_title'=>$this->sub_title,':summary'=>$str,':html_summary'=>$this->html_summary,':publish_status'=>$this->publication_status,':html_detaile'=>$this->html_detaile,':detaile'=>$str2,':image'=>$this->image));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
        }
        elseif(!empty($this->summary))  {
         $query = "UPDATE `owncms`.`article` SET `summary`=:summary WHERE `article`.`uniqid` = :uniqid";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':uniqid'=>$this->uniqid,':summary'=>$str));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
        }
        elseif(!empty($this->detaile))  {
         $query = "UPDATE `owncms`.`article` SET `detaile` =:detaile WHERE `article`.`uniqid` = :uniqid";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':uniqid'=>$this->uniqid,':detaile'=>$str2));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
        }
        elseif(empty($this->image))  {
         $query = "UPDATE `owncms`.`article` SET `title`=:title,`sub_title`=:sub_title,`summary`=:summary,`html_summary`=:html_summary,`publish_status`=:publish_status,`html_detaile`=:html_detaile,`detaile`=:detaile  WHERE `article`.`uniqid` = :uniqid";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':uniqid'=>$this->uniqid,':title'=>$this->title,':sub_title'=>$this->sub_title,':summary'=>$str,':html_summary'=>$this->html_summary,':publish_status'=>$this->publication_status,':html_detaile'=>$this->html_detaile,':detaile'=>$str2));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
        }
        
     elseif(empty($this->title)) {
        Utility::redirect();
        }
 else {
     Utility::redirect();
 }

 
    }
    
     public function delete($data="") {
       $query = "DELETE FROM `owncms`.`article` WHERE `article`.`uniqid` =:id" ;
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->uniqid));
        //var_dump($result);
        //die();
        if($result){
            Message::message("Data has bee deleted successfully");
            Utility::redirect();
        }
        else{
            Utility::redirect();
        }
         
         
     }
    
     public function show() {
         
         $query = "SELECT * FROM `owncms`.`article` WHERE `uniqid`=:id";
       
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$this->uniqid));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    public function trash() {
        $this->deleted_at=  time();
        $query = "UPDATE `owncms`.`article` SET `deleted_at`=:deleted_at WHERE `article`.`uniqid` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->uniqid,':deleted_at'=>$this->deleted_at));
         if($result){
         Message::message("Data has been trashed successfully");
         Utility::redirect();
       }
      
       
        else {
           Utility::redirect();
        }
    }
    public function trashed() {
        $allData = array();
        $query = "SELECT * FROM `owncms`.`article` WHERE `deleted_at` IS NOT NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
    }
    public function recover() {
//        var_dump($_GET);
//        die();
        $query = "UPDATE `owncms`.`article` SET `deleted_at`=:deleted_at WHERE `article`.`uniqid` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->uniqid,':deleted_at'=>NULL));
         if($result){
         Message::message("Data has been recovered successfully");
         Utility::redirect();
       }
      
       
        else {
           Utility::redirect();
        }
    }
    public function block_person() {
//        var_dump($_GET);
//        die();
        $query = "UPDATE `owncms`.`article` SET `block`=:block WHERE `article`.`uniqid` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->uniqid,':block'=>NULL));
         if($result){
         Message::message("Data has been recovered successfully");
         Utility::redirect();
    }}
    public function getALLtitle(){
        $allData = array();
        $query = "SELECT `title` FROM `owncms`.`article` WHERE `deleted_at` IS NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;

    }
    public function logout() {
        session_destroy();
        
       session_unset(Info::info('id'));
        Utility::redirect();
    }
    public function recent_post() {
//        var_dump($_GET);
//        die();
        
        $query = "SELECT * FROM `owncms`.`article` WHERE `deleted_at` IS NULL ORDER BY `created_at` LIMIT 0,3";
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
//        var_dump($allData);
//        die();
    }
    public function top_10_post() {
//        var_dump($_GET);
//        die();
        
        $query = "SELECT `title` FROM `owncms`.`article` WHERE `deleted_at` IS NULL ORDER BY `created_at` LIMIT 0,10";
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
//        var_dump($allData);
//        die();
    }
}

