<?php

namespace App\User;

class Utility{
    public static function d($data=''){
        echo "<pre>";
        echo var_dump($data);
        echo '</pre>';
    }
    
    public static function dd($data='') {
        echo "<pre>";
        echo var_dump($data);
        echo '</pre>';
        die();
    }
    
    public static function redirect() {
        header ('Location:../../index.php');
    }
    public static function redirect2() {
        header ('Location:home_page.php');
    }
    public static function redirect3() {//for confirmation code
        header('Location:http://localhost/BITM_Project/views/User_info/home_page.php');
    }
    public static function redirect4() {//if confirmation code don't match
        header('Location:http://localhost/BITM_Project/index.php');
    }
    public static function redirect5() {
        header ('Location:../../views/User_info/userpage.php');
    }
    public static function redirect6() {
        header ('Location:index.php');
    }
}