<?php

namespace App\Admin;
use App\Admin\Utility;
use App\Admin\Message;
use PDO;
class Users{
    public $id="";
    public $is_admin="";
    public $is_active="1";
    public $search="";
    public $username="";
    public $uniqid="";
    public $email="";
    public $conn;
    public $user='root';
    public $pass='';
    public $password='';
    public $deleted_at=""; 
    
    public $user_id="";
    public $firstname='';
    public $lastname='';
    public $gender='';
    public $mobile_no='';
    public $country='';
    public $image='';
    public $address='';
    
    public function __construct() {
        $this->conn = new PDO('mysql:host=localhost;dbname=owncms', $this->user, $this->pass);
        
    }
    public function prepare($data=array()){
//        var_dump($_POST);
//        die();
        if(is_array($data) && array_key_exists('username', $data)){
            $this->username=$data['username'];
        }
        if(is_array($data) && array_key_exists('email', $data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('id',$data) && !empty($data['id'])){
            $this->id=$data['id'];
        }
        if(array_key_exists('user_id',$data) && !empty($data['user_id'])){
            $this->user_id=$data['user_id'];
        }
        if(array_key_exists('is_admin',$data) && !empty($data['is_admin'])){
            $this->is_admin=$data['is_admin'];
        }
        
        if(is_array($data) && array_key_exists('is_active',$data)){
             $this->is_active=$data['is_active'];
        }
        if(is_array($data) && array_key_exists('password',$data)){
             if(!empty($_POST['password'])){//after update the password may not change
             $this->password=md5($data['password']);}
        }
        if(array_key_exists('uniqid',$data) && !empty($data['uniqid'])){
            $this->uniqid=$data['uniqid'];
        }

       if(array_key_exists('deleted_at', $data) && !empty($data['deleted_at'])){
           $this->deleted_at=$data['deleted_at'];
       }
       if (array_key_exists('search', $data) && !empty($data['search'])) {
            $this->search = $data['search'];
        }
        
        
        if (array_key_exists('firstname', $data) && !empty($data['firstname'])) {
            $this->firstname = $data['firstname'];
        }
        if (array_key_exists('lastname', $data) && !empty($data['lastname'])) {
            $this->lastname = $data['lastname'];
        }
        if (array_key_exists('gender', $data) && !empty($data['gender'])) {
            $this->gender = $data['gender'];
        }
        if (array_key_exists('address', $data) && !empty($data['address'])) {
            $this->address = $data['address'];
        }
        if (array_key_exists('mobile_no', $data) && !empty($data['mobile_no'])) {
            $this->mobile_no = $data['mobile_no'];
        }
        if (array_key_exists('country', $data) && !empty($data['country'])) {
            $this->country = $data['country'];
        }
        if (array_key_exists('image', $data) && !empty($data['image'])) {
            $this->image = $data['image'];
        }
        
      return $this;
//       var_dump($this);
//       die();
    }
    public function index() {
//        var_dump($_GET);
//        die();
        $allData = array();
        $whereClause=" 1=1 ";
        
        if(!empty($this->search)){
            $whereClause.="AND `username` LIKE '%{$this->search}%'";

        }

        
        $query = "SELECT * FROM `owncms`.`users` WHERE `deleted_at` IS NULL AND".$whereClause;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
//        var_dump($allData);
//        die();
    }
    public function index2() {//sinlg row mail
         $query = "SELECT * FROM `owncms`.`users` LEFT JOIN `profiles` on `users`.`id`= `profiles`.`user_id` WHERE `users`.`id`=:id";
        
        $result = $this->conn->prepare($query);
        
        $result->execute(array(':id'=>$this->id));
      
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        
        return $allData;
    }
    
    
     public function store() {
//         var_dump($this);
//         die();
         //$id=$_POST['id'];
         $uniq_id=  uniqid();
//         var_dump($uniq_id);
//         die();
             if (!empty($this->username) && !empty($this->email)) {//,`unique_id`,:uniqid,':uniqid'=>$uniq_id,
            $query = "INSERT INTO `owncms`.`users` (`username`,`email`,`password`,`is_active`,`uniqid`) VALUES (:username,:email,:password,:is_active,:uniqid)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':username'=>$this->username,':email'=>$this->email,':password'=>$this->password,':is_active'=>$this->is_active,':uniqid'=>$uniq_id));

            if ($result) {
                Message::message("Data has been stored successfully");
                Utility::redirect();
            }
            else {
            Utility::redirect();
        }
    }
        
        else {
            Utility::redirect();
        }
            
    }
    
     public function edit() {
//         var_dump($_GET);
//         die();
         $query = "SELECT * FROM `owncms`.`profiles` WHERE `profiles`.`user_id` = :id";
       
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$this->id));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
       
    }
    
     public function update() {//profile update, while admin updating a profile of any user
        
        //if(!empty($this->user_id))  {
         
         $query = "UPDATE `owncms`.`profiles` SET `firstname`=:firstname,`lastname` =:lastname,`gender` =:gender,`mobile_no` =:mobile_no,`address` =:address,`country` =:country,`image` =:image WHERE `profiles`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':firstname'=>$this->firstname,':lastname'=>$this->lastname,':gender'=>$this->gender,':mobile_no'=>$this->mobile_no,':address'=>$this->address,':country'=>$this->country,':image'=>$this->image));
        
         if($result){
             Message::message("Data has been updated successfully");
             Utility::redirect();
         }
         else{
             Utility::redirect();
         }
   }
   public function update_insert(){// admin creating a user profile while user profile isnot setted befor
//       var_dump($this);
//       die();
            $query = "INSERT INTO `owncms`.`profiles`(`firstname`,`lastname`,`mobile_no`,`address`,`country`,`gender`,`image`,`user_id`) VALUES (:firstname,:lastname,:mobile_no,:address,:country,:gender,:image,:user_id)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':firstname'=>$this->firstname,':lastname'=>$this->lastname,':mobile_no'=>$this->mobile_no,':address'=>$this->address,':country'=>$this->country,':gender'=>$this->gender,':image'=>$this->image,':user_id'=>$this->user_id));

            if ($result) {
                Message::message("Data has been stored successfully");
                Utility::redirect();
            }
            else {
            Utility::redirect();
        }

   }

    
     public function delete($data="") {
       $query = "DELETE FROM `owncms`.`users` WHERE `users`.`uniqid` =:id" ;
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->uniqid));
        //var_dump($result);
        //die();
        if($result){
            Message::message("Data has bee deleted successfully");
            Utility::redirect();
        }
        else{
            Utility::redirect();
        }
         
         
     }
    
     public function show() {//SELECT * FROM `user` LEFT JOIN `user_profile` on `user`.`id`= `user_profile`.`user_id` 
         
         $query = "SELECT * FROM `owncms`.`users` LEFT JOIN `profiles` on `users`.`id`= `profiles`.`user_id` WHERE `users`.`uniqid`=:id";
       
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$this->uniqid));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    public function trash() {
        $this->deleted_at=  time();
        $query = "UPDATE `owncms`.`users` SET `deleted_at`=:deleted_at WHERE `users`.`uniqid` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->uniqid,':deleted_at'=>$this->deleted_at));
         if($result){
         Message::message("Data has been trashed successfully");
         Utility::redirect();
       }
      
       
        else {
           Utility::redirect();
        }
    }
    public function trashed() {
        $allData = array();
        $query = "SELECT * FROM `owncms`.`users` WHERE `deleted_at` IS NOT NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
    }
    public function recover() {
//        var_dump($_GET);
//        die();
        $query = "UPDATE `owncms`.`users` SET `deleted_at`=:deleted_at WHERE `users`.`uniqid` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->uniqid,':deleted_at'=>NULL));
         if($result){
         Message::message("Data has been recovered successfully");
         Utility::redirect();
       }
      
       
        else {
           Utility::redirect();
        }
    }
    public function getALLusername(){
        $allData = array();
        $query = "SELECT `username` FROM `owncms`.`users` WHERE `deleted_at` IS NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;

    }
    public function logout() {
        session_destroy();
        
       session_unset(Info::info('id'));
        Utility::redirect();
    }
}

